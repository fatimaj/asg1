﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


// Group 2, Assignment 5, Oct 9, 2018
namespace PopulationEstimate
{
    /// <summary>
    /// Handles the the Xaml Window events and sets up the ViewModel for the Xaml Window 
    /// </summary>
    public partial class MainWindow : Window
    {
        //initializing the VM class Object
        ViewModel vm = new ViewModel();

        public MainWindow()
        {
            InitializeComponent();
            DataContext = vm;
        }

        // event handler for Calculate button click. Calls the Calcualte methos from the vm object class.
        //Enable the save button and clear button.
        private void BtnCalculate_Click(object sender, RoutedEventArgs e)
        {
            BtnSave.IsEnabled = true;
            BtnClear.IsEnabled = true;
            vm.CalculatePopulation();
        }

        //event handler for Clear button click. Calls the Clear method from the vm object class.
        // Disable Clear button and enable Save button.
        private void BtnClear_Click(object sender, RoutedEventArgs e)
        {
            BtnClear.IsEnabled = false;
            BtnSave.IsEnabled = false;
            BtnCalculate.IsEnabled = true;
            vm.Clear();
        }

        //event handler for Save button click. Calles the Save method from the vm object class
        // Disable save button and enable clear button. 
        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            BtnSave.IsEnabled = false;
            BtnClear.IsEnabled = true;
            vm.Save();
        }
    }
}
