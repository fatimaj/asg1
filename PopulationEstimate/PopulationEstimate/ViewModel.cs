﻿using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;

// Group 2, Assignment 5, Oct 9, 2018
namespace PopulationEstimate
{
    class ViewModel : INotifyPropertyChanged
    {
        #region PropertyDefinition

        // Define properties used for interaction with XAML sheet. Includes textboxes for input, indexes of list box, and labels for output 
        private BindingList<string> data = new BindingList<string>();
        public BindingList<string> Data
        {
            get { return data; }
            set { data = value; NotifyChanged(); }
        }

        private uint startingPopulation = 0;
        public uint StartingPopulation
        {
            get { return startingPopulation; }
            set { startingPopulation = value; NotifyChanged(); }
        }

        private string startingPopulationInfo = "Initial population:";
        public string StartingPopulationInfo
        {
            get { return startingPopulationInfo; }
            set { startingPopulationInfo = value; NotifyChanged(); }
        }

        private float percentIncrease = 0;
        public float PercentIncrease
        {
            get { return percentIncrease; }
            set { percentIncrease = value; NotifyChanged(); }
        }

        private uint numberOfDays = 0;
        public uint NumberOfDays
        {
            get { return numberOfDays; }
            set { numberOfDays = value; NotifyChanged(); }
        }

        private bool backwardCount;
        public bool BackwardCount
        {
            get { return backwardCount; }
            set { backwardCount = value; Clear(); NotifyChanged(); }
        }

        #endregion

        #region Logic

        //a method that calculates the total population (given starting popuation) of an organism.
        //The function requires a daily increase rate and # of days to multiply for the calculation.
        public void CalculatePopulation()
        {
            //constants for later use
            const int HUNDRED = 100;
            const int NUMBER_ONE = 1;

            // Variables
            float currentPopulation = StartingPopulation;

            // binding list cleanup and adding description element
            Data.Clear();
            Data.Add("Day #, Population");

            //loop that calculates the population depending on the radio button selected and displays on labels
            for (int Index = 0; Index < NumberOfDays; Index++)
            {
                if (BackwardCount)
                {
                    currentPopulation = (currentPopulation / (NUMBER_ONE + (percentIncrease / HUNDRED)));
                    Data.Add($"Day {NumberOfDays - Index}, {Math.Ceiling(currentPopulation)}");
                }
                else
                {
                    currentPopulation += (currentPopulation * (PercentIncrease / HUNDRED));
                    Data.Add($"Day {Index + 1}, {Math.Floor(currentPopulation)}");
                }
            }
        }

        // Method clearing various labels, textboxes and listboxes 
        public void Clear()
        {
            StartingPopulationInfo = $"{(BackwardCount ? "Final" : "Initial")} population";
            Data.Clear();
            StartingPopulation = 0;
            NumberOfDays = 0;
            PercentIncrease = 0;
        }

        // the method that saves the calculated population output to a file.
        public void Save()
        {
            string fileText = "";

            // Appending the population data to a string
            for (int Index = 0; Index < NumberOfDays + 1; Index++)
            {
                fileText += Data[Index];
                fileText += Environment.NewLine;
            }

            // setting up the app, data and file path
            string appPath = Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData);
            string dataPath = Path.Combine(appPath, "PROG8010");
            string filePath = Path.Combine(dataPath, "output.txt");
            if (!Directory.Exists(dataPath))
            {
                Directory.CreateDirectory(dataPath);
            }

            // Appending population data to a file at the provided path
            File.AppendAllText(filePath, fileText);
        }
        #endregion

        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyChanged([CallerMemberName] string name = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
        #endregion
    }
}
